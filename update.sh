#!/usr/bin/env bash
#
# script to update ssh auth keys on system

set -o errexit
set -o pipefail
set -o nounset
[ -n "${TRACE-}" ] && set -o xtrace

# set some variables
# DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR=${PWD}


# functions anyone
folder_check () {
    if [ ! -d "${HOME}/.ssh" ]; then
        mkdir -pv "${HOME}"/.ssh
        chmod 0700 "${HOME}"/.ssh
    fi
}


# delete auth file if set
file_reset () {
    if [[ "${RESET-}" == "1" ]]; then
        rm -rf "${HOME}"/.ssh/authorized_keys
    fi
}


file_check () {
    if [ ! -f "${HOME}/.ssh/authorized_keys" ]; then
        touch "${HOME}"/.ssh/authorized_keys
        chmod 0600 "${HOME}"/.ssh/authorized_keys
    fi
}


config_check () {
    if [ ! -f "${HOME}/.ssh/config" ]; then
        cp "${DIR}"/config "${HOME}"/.ssh/
        chmod 0600 "${HOME}"/.ssh/config
    fi
}


key_check () {
    local FILENAME
    local KEY

    for FILENAME in "${DIR}"/*.pub
    do
        KEY=$(cat "${FILENAME}"); \
        grep -q "${KEY}" "${HOME}"/.ssh/authorized_keys || \
        echo "${KEY}" >> "${HOME}"/.ssh/authorized_keys
    done
}


main () {
    folder_check
    file_reset
    file_check
    config_check
    key_check
}

# lets go
main "${@-}"